<?php

namespace System;

/**
 * Description of DataBaseStorage
 *
 * @author Dmitriy
 */
class DataBaseStorage implements Storage
{

    protected $model;
    
    protected $dateformat = 'Y-m-d H:i:s';
    
    function __construct($model)
    {
        $this->model = $model;
    }

    //put your code here
    public function delete($id)
    {
        $model = $this->model;
        return $model::table()->delete($id);
    }

    /**
     * сохранение массива
     * 
     * @param type $data
     */
    public function persist(array $data)
    {
        $model = $this->model;
        $pkey = $model::$primary_key;
   
        if(null === ($id = $data['id'])){
            $id = $model::create($data)->{$pkey};           
        }
        else{
            unset($data['id']);
            $model_find = $model::find($id);
            $model_find->update_attributes($data);
        }
            
        return $id;
    }

    /**
     * Воставновление записи их бд
     * @param type $id
     * @return type
     */
    public function retrieve($id)
    {
        $model = $this->model;
        return $this->convertAliases($model::find($id));
    }

    /**
     * Переводим имена полей pos_id => id
     * 
     * @param array $data
     * @return array
     */
    protected function convertAliases(\ActiveRecord\Model $rmodel)
    {
        $new_ar = [];
        $model = $this->model;

        $attr = array_flip($rmodel::$alias_attribute);

        foreach ($attr as $k => $v) {
            if (isset($rmodel->$k) && is_a($rmodel->$k,'ActiveRecord\DateTime')){
                $new_ar[$v] = $rmodel->$k->format($this->dateformat);
            }
            elseif (isset($rmodel->$k)){
                $new_ar[$v] = $rmodel->$k;
            } 
        }
        return $new_ar;
    }
}
