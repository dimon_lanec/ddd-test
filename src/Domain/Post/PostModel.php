<?php

namespace System\Domain\Post;

use ActiveRecord\Model;

/**
 * объекта доступа слоя DataBase
 * 
 * @codeCoverageIgnore
 * @author Dmitriy
 */
final class PostModel extends Model
{

    /**
     * таблица
     * @var type
     */
    public static $table_name = 'posts';

    /**
     * ключ
     * @var type
     */
    public static $primary_key = 'pos_id';

    /**
     * aliases
     * @var type
     */
    public static $alias_attribute = [
        'id'        => 'pos_id',
        'author'    => 'pos_author',
        'created'   => 'pos_created',
        'text'      => 'pos_text',
        'title'     => 'pos_title',
    ];

}
