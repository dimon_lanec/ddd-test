<?php

namespace System\Domain\Post;

use System\Storage,
    System\IntityRepository;

/**
 * Repository for class Post
 * Этот класс находится между Entity слоя (класса столба) и 
 * объекта доступа слоя (интерфейс хранения)  
 * Repository инкапсулирует набор объектов сохраняется в хранилище данных и операции, 
 * выполняемые над ними  
 * Предоставление более объектно-ориентированный вид на сохранение слоя 
 * Repository также поддерживает цель достижения четкое разделение и одностороннюю зависимость  
 * Между картографических слоев домена и данных
 *
 * - Данный слой можно тестировать 
 * 
 * Class PostRepository
 * @package DesignPatterns\Repository
 */
class PostRepository extends IntityRepository
{





}
