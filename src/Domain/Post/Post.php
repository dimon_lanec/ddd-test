<?php
namespace System\Domain\Post;

use System\Intity;
/**
 * Сообщение представляет объект по каким-то пост, который пользователь остается на сайте
 *
 * Entity слоя
 * 
 * - Данный слой можно тестировать 
 * 
 * Class Post
 */
class Post extends Intity
{

    /**
     * @var string
     */
    protected $title;

    /**
     * @var string
     */
    protected $text;

    /**
     * @var string
     */
    protected $author;

    /**
     * @var \DateTime
     */
    protected $created;

    function __construct($title,$author)
    {
        $this->setTitle($title);
        $this->setAuthor($author);
    }
    
    /**
     * @param string $author
     */
    public function setAuthor($author)
    {
        $this->author = $author;
    }

    /**
     * @return string
     */
    public function getAuthor()
    {
        return $this->author;
    }

    /**
     * @param \DateTime $created
     */
    public function setCreated($created)
    {
        $this->created = $created;
    }

    /**
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * @param string $text
     */
    public function setText($text)
    {
        $this->text = $text;
    }

    /**
     * @return string
     */
    public function getText()
    {
        return $this->text;
    }

    /**
     * @param string $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }
}