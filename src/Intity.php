<?php

namespace System;

/**
 * Сущность
 *
 * @author Dmitriy
 */
abstract class Intity
{
    /**
     * @var int
     */
    protected $id;
    
    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }    
}
