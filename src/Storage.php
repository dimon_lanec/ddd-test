<?php
namespace System;
/**
 * Interface Storage
 *
 * Этот интерфейс описывает методы для получения доступа хранения.
 * Реализация бетона может быть все, что мы хотим - в памяти, реляционная база данных, 
 * NoSQL базы данных и т.д.
 * @package DesignPatterns\Repository
 */
interface Storage
{
    /**
     * Method to persist data
     * Returns new id for just persisted data
     *
     * @param array() $data
     * @return int
     */
    public function persist(array $data);

    /**
     * Returns data by specified id.
     * If there is no such data null is returned
     *
     * @param int $id
     * @return array|null
     */
    public function retrieve($id);

    /**
     * Delete data specified by id
     * If there is no such data - false returns, if data has been successfully deleted - true returns
     *
     * @param int $id
     * @return bool
     */
    public function delete($id);
}