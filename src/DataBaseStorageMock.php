<?php

namespace System;

/**
 * Description of DataBaseStorage
 * @codeCoverageIgnore
 * @author Dmitriy
 */
class DataBaseStorageMock implements Storage
{

    static $primary_key = 'id';
    
    static public function table()
    {
        return new DataBaseStorageMock;
    }

    public function delete($id)
    {
        return true;
    }
    
    static public function create(){
        $o = new DataBaseStorageMock;
        $o->id = 1;
        return $o;
    }    

    public function persist(array $data)
    {
        return 1;
    }

    public function retrieve($id)
    {
        
    }
}
