<?php

namespace System;

/**
 * Аюстрактный класс IntityRepository
 * содержит CRUD операции для сущности
 * @author Dmitriy
 */
abstract class IntityRepository
{
    protected $persistence;

    protected $builder;
    
    /**
     * указываем модель и функцию для создания объекта
     * @param Storage $persistence
     * @param type $fn
     */
    public function __construct(Storage $persistence, $fn)
    {
        $this->persistence = $persistence;
        $this->builder = $fn;
    }

    /**
     * функция для создания объекта
     * @return type
     */
    public function build()
    {
        return call_user_func($this->builder);
    }
    
    /**
     * Получение записи по id
     *
     * @param int $id
     * @return Post|null
     */
    public function getById($id)
    {
        $arrayData = $this->persistence->retrieve($id);
        if (is_null($arrayData)) {
            return null;
        }

        $post = $this->build();
        return $this->build_object($post, $arrayData);
    }    
    
    /**
     * Создание объекта из массива
     * @param type $ModelIntity
     * @param type $ar
     * @param object $ModelIntity
     */
    protected function build_object(Intity $ModelIntity, $ar = [])
    {
        foreach ($ar as $key => $value) {
            if (method_exists($ModelIntity, 'set' . $key)) {
                $ModelIntity->{'set' . $key}($value);
            }
        }
        return $ModelIntity;
    }

    /**
     * Создание массива из объекта 
     * @param type $ModelIntity
     * @param type $ar
     * @return type
     */
    protected function build_array(Intity $ModelIntity)
    {
        $data = [];

        foreach (get_class_methods($ModelIntity) as $method) {
            if (preg_match('~get(.*)$~isu', $method, $m)) {

                $value = $ModelIntity->$method();
                $field = strtolower($m[1]);

                if ($field == 'id' && empty($value))
                    continue;

                $data[$field] = $value;
            }
        }
        return $data;
    }    
    
    /**
     * Сохранение объекта
     *
     * @param Post $post
     * @return Post
     */
    public function save(Intity $post)
    {
        $id = $this->persistence->persist($this->build_array($post));
       
        if($id && $post->getId()===null){
			$post->setId($id);
		}
            
        return $post;
    }

    /**
     * Deletes specified Post object
     *
     * @param Post $post
     * @return bool
     */
    public function delete(Intity $post)
    {
        return $this->persistence->delete($post->getId());
    }    
}
