<?php
require_once __DIR__ . '/vendor/autoload.php';

use ActiveRecord\Config as DbConfig,
    System\DataBaseStorage,
    System\Domain\Post\PostRepository,
    System\Domain\Post\Post;

function dump($d,$e=false){
         echo '<pre>';
        var_dump($d);
        echo '</pre>';
        if($e==true)die('stop');
}



DbConfig::initialize(function($cfg)
 {
    $cfg->set_connections(array('development' =>
        'mysql://root:@localhost/diamond'));
 });

//$storage = new \System\MemoryStorage;
$storage = new DataBaseStorage('System\Domain\Post\PostModel');

$repo = new PostRepository($storage, function(){
    return new Post('Новость', 'Dima');
});

$post = $repo->build();


//добавление новой записи
//$post->setText('текст...');
//$post = $repo->save($post);

//обновление записи
//$post->setId(5);
//$post->setText('текст.222..');
//$post = $repo->save($post);

//получение записи по id
//$post2 = $repo->getById($post->getId());

echo '<pre>';
print_r($post);
echo '</pre>';
